﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using API_DATAACCESSS.EN;


namespace API_DATAACCESSS.BL
{
    public class CategorieBL
    {
        static String _conn = @"Data Source=(local);Initial Catalog=Northwind;Integrated Security=True";

        public IEnumerable<ENCategorie> GetAllCategorie()
        {
            IDbConnection conn = new SqlConnection(_conn);
            conn.Open();
            var query = conn.Query<ENCategorie>("SELECT [CategoryID],[CategoryName],[Description] CategoryDescription FROM[dbo].[Categories]");
            conn.Close();

            return query;
        }

        public IEnumerable<ENCategorie> GetCategorie(int Id)
        {
            IDbConnection conn = new SqlConnection(_conn);
            conn.Open();
            var query = conn.Query<ENCategorie>($"SELECT [CategoryID],[CategoryName],[Description] CategoryDescription " +
                                                $"FROM[dbo].[Categories] " +
                                                $"WHERE [CategoryID] = {Id}");
            conn.Close();
            

            return query;
        }

        public bool UpdateCategorie(ENCategorie categorie)
        {
            int Id = categorie.CategoryID;
            string Description = categorie.CategoryDescription;         
            try
            {
                string sql = $"UPDATE [dbo].[Categories] SET[Description] = '{ Description}' " +
                             $"WHERE CategoryID = {Id}";
                IDbConnection conn = new SqlConnection(_conn);
                conn.Open();
                conn.Execute(sql,new { CategoryID = Id, Description = Description});
                conn.Close();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }

        public bool InsertCategorie(ENCategorie categorie)
        {            
            string category = categorie.CategoryDescription;
            string Description = categorie.CategoryDescription;
            try
            {
                string sql = $"EXEC dbo.CreateCategory @CategoryName,@Description";
                var values = new { CategoryName = category,Description = Description };                                             
                IDbConnection conn = new SqlConnection(_conn);
                conn.Open();
                var results = conn.Query(sql, values);                                
                conn.Close();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }

        }

    }
}
