﻿using System;
using System.Collections.Generic;
using System.Text;
using API_DATAACCESSS.BL;


namespace API_DATAACCESSS
{
    public class Categorie
    {
        public string GetMessagge()
        {
            return "data access categorie";
        }

        public IEnumerable<EN.ENCategorie> GetAllCategorie()
        {
            CategorieBL categorie = new CategorieBL();
            return categorie.GetAllCategorie();
        }

        public IEnumerable<EN.ENEmployee> GetEmployee()
        {
            EN.ENEmployee[] employees = new EN.ENEmployee[]
            {
                new EN.ENEmployee{ FirstName = "pepe",LastName = "pepe" },
                new EN.ENEmployee{ FirstName = "pedro",LastName = "pedro" },
                new EN.ENEmployee{ FirstName = "lucas",LastName = "lucas" }
            };

            return employees;
        }


        public IEnumerable<EN.ENCategorie> GetCategorie(int Id)
        {
            CategorieBL categorie = new CategorieBL();
            return categorie.GetCategorie(Id);
        }


        public bool UpdateCategorie(EN.ENCategorie category)
        {
            CategorieBL categorie = new CategorieBL();
            return categorie.UpdateCategorie(category);
        }

        public bool InsertCategorie(EN.ENCategorie category)
        {
            CategorieBL categorie = new CategorieBL();
            return categorie.InsertCategorie(category);
        }

    }
}
