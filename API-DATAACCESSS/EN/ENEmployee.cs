﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_DATAACCESSS.EN
{
    public class ENEmployee
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }

        public string Title { get; set; }

        public int Age { get; set; }

    }
}
