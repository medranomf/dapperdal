﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_DATAACCESSS.EN
{
    public class ENCategorie
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
    }
}
