﻿using API_DATAACCESSS;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_DATAACCESSS.EN;

namespace API_Nortwhind.Controllers
{
    [ApiController]

    public class CategorieController : Controller
    {
       
        [HttpGet]
        [Route("categorie/getmessage")]
        public string GetMessagge()
        {
            Categorie category = new Categorie();
            return category.GetMessagge();
        }

        [HttpGet]
        [Route("categorie/getallcategorie")]
        public IEnumerable<ENCategorie> GetAllCategorie()
        {
            Categorie category = new Categorie();
            return category.GetAllCategorie();
        }

        [HttpGet]
        [Route("categorie/getallcategorie")]
        public IEnumerable<ENCategorie> GetAllEmployee()
        {
            Categorie category = new Categorie();
            return category.GetAllCategorie();
        }

        [HttpGet]
        [Route("categorie/getcategorie/{Id}")]
        public IEnumerable<ENCategorie> GetCategorie(int Id)
        {
            Categorie category = new Categorie();
            return category.GetCategorie(Id);
        }

        [HttpPost]
        [Route("categorie/updatecategorie")]
        public bool UpdateCategorie([FromBody] ENCategorie category)
        {
            Categorie categorie = new Categorie();

            return categorie.UpdateCategorie(category);
        }


        [HttpPost]
        [Route("categorie/insertcategorie")]
        public bool InsertCategorie([FromBody] ENCategorie category)
        {
            Categorie categorie = new Categorie();

            return categorie.InsertCategorie(category);
        }



    }
}
